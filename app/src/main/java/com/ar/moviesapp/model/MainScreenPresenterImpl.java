package com.ar.moviesapp.model;

import android.content.Context;
import android.util.Log;

import com.ar.moviesapp.ErrorCodeConstants;
import com.ar.moviesapp.Utils;
import com.ar.moviesapp.apis.RestClient;
import com.ar.moviesapp.apis.model.MoviesModel;
import com.ar.moviesapp.presenter.MainScreenPresenter;
import com.ar.moviesapp.view.ActivityView;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

import static com.ar.moviesapp.ErrorCodeConstants.SERVER_ERROR;

/**
 * Created by abdul on 16/04/18.
 */

public class MainScreenPresenterImpl implements MainScreenPresenter {
    ActivityView mainActivityView;

    public MainScreenPresenterImpl(ActivityView mainScreenView) {
        this.mainActivityView = mainScreenView;
    }

    @Override
    public void doCallOrErrorHandle(int code, int page) {
        if (code == ErrorCodeConstants.SUCCESS) {
            doRestApiCall(page);
        } else {
            Log.d("presenter", "error:" + code);
            mainActivityView.error(code);
        }
    }

    @Override
    public void validateAndProceed(Context mContext, int page) {
        if (Utils.isNetworkAvailable(mContext))
            doCallOrErrorHandle(ErrorCodeConstants.SUCCESS, page);
        else
            doCallOrErrorHandle(ErrorCodeConstants.INTERNET_CONNECTION, page);
    }

    private void doRestApiCall(int page) {
        final CompositeDisposable disposable = new CompositeDisposable();
        RestClient.getApiService().getMoviesList(page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<MoviesModel>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable.add(d);
                        Log.d("onSubscribe", "");
                    }

                    @Override
                    public void onNext(Response<MoviesModel> response) {
                        Log.d("onNext", response.code() + "");
                        if (response.code() == 200)
                            mainActivityView.getServerResponse(response);
                        else
                            mainActivityView.error(SERVER_ERROR);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("onError", e.getMessage());
                        mainActivityView.error(SERVER_ERROR);
                    }

                    @Override
                    public void onComplete() {
                        disposable.clear();
                        Log.d("onComplete", "");
                    }
                });
    }
}
