package com.ar.moviesapp.view;


import com.ar.moviesapp.apis.model.MoviesModel;

import retrofit2.Response;

/**
 * Created by abdul on 16/04/18.
 */

public interface ActivityView {
    void error(int type);

    void getServerResponse(Response<MoviesModel> response);
}
