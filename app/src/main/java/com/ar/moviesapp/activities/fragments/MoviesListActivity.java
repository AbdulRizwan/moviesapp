package com.ar.moviesapp.activities.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.ar.moviesapp.FontUtils;
import com.ar.moviesapp.R;
import com.ar.moviesapp.Utils;
import com.ar.moviesapp.adapters.MoviesListAdapter;
import com.ar.moviesapp.apis.model.MoviesInfo;
import com.ar.moviesapp.apis.model.MoviesModel;
import com.ar.moviesapp.model.MainScreenPresenterImpl;
import com.ar.moviesapp.presenter.MainScreenPresenter;
import com.ar.moviesapp.view.ActivityView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

import static android.graphics.Typeface.BOLD;
import static com.ar.moviesapp.ErrorCodeConstants.INTERNET_CONNECTION;
import static com.ar.moviesapp.ErrorCodeConstants.SERVER_ERROR;

/**
 * Created by abdul on 16/04/18.
 * this is fragment which is initiated by Activity;
 * 1. setup views
 * 2. loadMoviesList() method, which will increase page number with infinite call;
 * 3. MVP pattern used for validate and make a call using interface;
 */

public class MoviesListActivity extends AppCompatActivity implements ActivityView, View.OnClickListener {

    private View rootView, mErrorView, resultView;
    MainScreenPresenter presenter;
    View progressBarView;
    Button retry;

    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;

    private RecyclerView recyclerView;
    private StaggeredGridLayoutManager layoutManager;
    private MoviesListAdapter adapter;
    private List<MoviesInfo> upcomingMoviesList;
    private String TAG = this.getClass().getName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_fragment_layout);
        init();
        setupListeners();
        loadMoviesList();
    }


    private void loadMoviesList() {//now it will use gps/network
        page = (page % (totalPage + 1));
        page++;
        Log.d(TAG, "" + (page) + " totalPage:" + totalPage);
        presenter.validateAndProceed(this, page);
    }

    private void setupListeners() {
        retry.setOnClickListener(this);
    }

    private void init() {
        presenter = new MainScreenPresenterImpl(this);
        rootView = findViewById(R.id.rootView);
        progressBarView = findViewById(R.id.progress_bar);
        mErrorView = findViewById(R.id.error_view);
        resultView = findViewById(R.id.resultView);
        setupErrorView();
        setupResultView();
    }

    int page = 0, totalPage = 0;

    private void setupResultView() {
        recyclerView = resultView.findViewById(R.id.recyclerViewList);
        upcomingMoviesList = new ArrayList<>();
        adapter = new MoviesListAdapter(this, upcomingMoviesList);
        layoutManager = new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        upcomingMoviesList.clear();

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    int[] firstVisibleItems = null;

                    firstVisibleItems = layoutManager.findFirstVisibleItemPositions(firstVisibleItems);

                    if(firstVisibleItems != null && firstVisibleItems.length > 0) {
                        pastVisiblesItems = firstVisibleItems[0];
                    }

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            loadMoviesList();
                            Utils.showViews(progressBarView);
                        }
                    }
                }
            }
        });
    }

    private void setupViewAdapter(MoviesModel info) {
        adapter.addAllItem(info.getUpcomingMoviesList());
        loading = true;
        Utils.hideViews(progressBarView);
    }

    private void setupErrorView() {
        snackbar = Snackbar.make(rootView, R.string.server_error, Snackbar.LENGTH_INDEFINITE);
        retry = mErrorView.findViewById(R.id.buttonRetry);
        retry.setTypeface(FontUtils.getLight(), BOLD);
    }

    Snackbar snackbar;

    @Override
    public void error(int type) {
        dismissSnackBar();
        switch (type) {
            case SERVER_ERROR:
                Utils.hideViews(progressBarView, resultView);
                Utils.showViews(mErrorView);
                snackbar.show();
                break;
            case INTERNET_CONNECTION:
                Utils.hideViews(progressBarView, resultView);
                Utils.showViews(mErrorView);
                snackbar = Snackbar.make(rootView, R.string.network_error, Snackbar.LENGTH_INDEFINITE);
                snackbar.show();
                break;
        }
    }

    private void dismissSnackBar() {
        if (snackbar != null && snackbar.isShown()) {
            snackbar.dismiss();
        }
    }

    @Override
    public void getServerResponse(Response<MoviesModel> response) {
        Log.d("mainActivity", "data getting: ");
        Utils.hideViews(progressBarView, mErrorView);
        Utils.showViews(resultView);
        page = response.body().getPage();
        totalPage = response.body().getTotalPages();
        setupViewAdapter(response.body());
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonRetry:
                Utils.hideViews(mErrorView, resultView);
                Utils.showViews(progressBarView);
                dismissSnackBar();
                loadMoviesList();
                break;
        }
    }
}
