package com.ar.moviesapp.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.ar.moviesapp.R;
import com.ar.moviesapp.activities.fragments.MoviesListActivity;

/***
 * this class creating fragment, fragment will call the api to load information.
 */

//public class MainActivity extends AppCompatActivity {
//    String TAG = this.getClass().getName();
//
//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.main_layout);
//        setupFragment();
//    }
//
//    private void setupFragment() {
//        getSupportFragmentManager().beginTransaction().add(R.id.containerView, new MoviesListActivity()).commit();
//    }
//}