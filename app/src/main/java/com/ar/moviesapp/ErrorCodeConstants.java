package com.ar.moviesapp;

/**
 * Created by abdul on 09/04/18.
 */

public interface ErrorCodeConstants {
    int SERVER_ERROR = 2;
    int SUCCESS = 1;
    int INTERNET_CONNECTION = 4;
}
