package com.ar.moviesapp;

import android.content.Context;
import android.net.ConnectivityManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by abdul on 16/04/18.
 */

public class Utils {
    public static DecimalFormat df = new DecimalFormat("##");
    private static String TAG = "Utils";
    static DateFormat DEFAULT_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    public static final DateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("dd MMM yyyy");

    public static void hideViews(View... views) {

        for (View view : views) {
            if (view == null) continue;
            view.setVisibility(View.GONE);
        }
    }

    public static void showViews(View... views) {

        for (View view : views) {
            if (view == null) continue;
            view.setVisibility(View.VISIBLE);
        }
    }

    public static String getDateFormat(String dateTime) throws ParseException {
        Date date = DEFAULT_FORMAT.parse(dateTime);
        return SIMPLE_DATE_FORMAT.format(date);
    }

    public static void loadImage(Context context, ImageView imageView, String url, int placeHolderUrl, int errorImageUrl) {
        Log.d(TAG, "" + url);
        Glide.with(context)
                .load(url)
                .placeholder(placeHolderUrl)
                .error(errorImageUrl)
                .fitCenter()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageView);

    }

    /***
     * @param context passing to get the network status
     * @return boolean value as true or false
     * true: if network is available
     * false: if not available
     */
    public static boolean isNetworkAvailable(final Context context) {
        if (context == null) return false;
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        if (connectivityManager.getActiveNetworkInfo() != null) {
            try {
                return connectivityManager.getActiveNetworkInfo().isConnected();
            } catch (Exception e) {
            }
        }
        return false;
    }

}
