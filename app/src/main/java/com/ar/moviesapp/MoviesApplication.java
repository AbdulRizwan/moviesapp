package com.ar.moviesapp;

import android.app.Application;
import android.content.Context;

/**
 * Created by abdul on 17/04/18.
 */

public class MoviesApplication extends Application {

    private Context context;

    @Override
    public void onCreate() {
        context = this;
        FontUtils.initFonts(context);
        super.onCreate();
    }

}