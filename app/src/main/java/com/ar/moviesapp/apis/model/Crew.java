package com.ar.moviesapp.apis.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.squareup.moshi.Json;

/**
 * Created by abdul on 17/04/18.
 */

public class Crew implements Parcelable {
    @Json(name = "credit_id")
    private String creditId;

    @Json(name = "department")
    private String department;

    @Json(name = "gender")
    private Integer gender;

    @Json(name = "id")
    private Integer id;

    @Json(name = "name")
    private String name;

    @Json(name = "profile_path")
    private String profilePath;

    public String getCreditId() {
        return creditId;
    }

    public void setCreditId(String creditId) {
        this.creditId = creditId;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfilePath() {
        return profilePath;
    }

    public void setProfilePath(String profilePath) {
        this.profilePath = profilePath;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.creditId);
        dest.writeString(this.department);
        dest.writeValue(this.gender);
        dest.writeValue(this.id);
        dest.writeString(this.name);
        dest.writeString(this.profilePath);
    }

    public Crew() {
    }

    protected Crew(Parcel in) {
        this.creditId = in.readString();
        this.department = in.readString();
        this.gender = (Integer) in.readValue(Integer.class.getClassLoader());
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.name = in.readString();
        this.profilePath = in.readString();
    }

    public static final Parcelable.Creator<Crew> CREATOR = new Parcelable.Creator<Crew>() {
        @Override
        public Crew createFromParcel(Parcel source) {
            return new Crew(source);
        }

        @Override
        public Crew[] newArray(int size) {
            return new Crew[size];
        }
    };
}
