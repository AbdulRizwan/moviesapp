package com.ar.moviesapp.apis.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.squareup.moshi.Json;

import java.util.List;

/**
 * Created by abdul on 17/04/18.
 */

public class CastAndCrew implements Parcelable {
    @Json(name = "id")
    int id;

    @Json(name = "cast")
    List<Cast> castList;


    @Json(name = "crew")
    List<Crew> crewList;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeTypedList(this.castList);
        dest.writeTypedList(this.crewList);
    }

    protected CastAndCrew(Parcel in) {
        this.id = in.readInt();
        this.castList = in.createTypedArrayList(Cast.CREATOR);
        this.crewList = in.createTypedArrayList(Crew.CREATOR);
    }

    public static final Parcelable.Creator<CastAndCrew> CREATOR = new Parcelable.Creator<CastAndCrew>() {
        @Override
        public CastAndCrew createFromParcel(Parcel source) {
            return new CastAndCrew(source);
        }

        @Override
        public CastAndCrew[] newArray(int size) {
            return new CastAndCrew[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Cast> getCastList() {
        return castList;
    }

    public void setCastList(List<Cast> castList) {
        this.castList = castList;
    }

    public List<Crew> getCrewList() {
        return crewList;
    }

    public void setCrewList(List<Crew> crewList) {
        this.crewList = crewList;
    }
}
