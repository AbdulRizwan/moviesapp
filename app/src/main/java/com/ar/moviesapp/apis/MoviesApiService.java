package com.ar.moviesapp.apis;


import com.ar.moviesapp.apis.model.CastAndCrew;
import com.ar.moviesapp.apis.model.MoviesModel;
import com.ar.moviesapp.constants.Constants;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * using for retrofit.
 * Created by abdul on 16/04/18.
 */

public interface MoviesApiService {
    /***
     * this method using to fetch list of movies based on pagination;
     * @param page default is 1;
     * @return list of results , totalPageCount etc;
     */
    @GET("3/movie/upcoming?api_key=" + Constants.KEY)
    Observable<Response<MoviesModel>> getMoviesList(@Query("page") int page);


    /***
     * to get crew and cast information must need to pass movie id then get those informations;
     * @param movieId must have to pass to get crew and cast information;
     * @return list of crews and cast information;
     */
    @GET("3/movie/{movie_id}/credits?api_key=" + Constants.KEY)
    Observable<Response<CastAndCrew>> getCastAndCrew(@Path("movie_id") int movieId);
}
