package com.ar.moviesapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ar.moviesapp.FontUtils;
import com.ar.moviesapp.R;
import com.ar.moviesapp.Utils;
import com.ar.moviesapp.apis.model.Cast;
import com.ar.moviesapp.apis.model.Crew;

import java.util.List;

import static android.graphics.Typeface.BOLD;

/**
 * Created by abdul on 17/04/18.
 *
 * This adapter is using from MoviesListAdapter to load cast and crew information.
 * 1. same adapter is handle for both kind of view for cast and crew.
 * 2. using type handling two different scope of views;
 */

public class CastAndCrewAdapter extends RecyclerView.Adapter<CastAndCrewAdapter.MyViewHolder> {

    List<Cast> itemCast;
    List<Crew> itemCrew;
    int type;
    public static int CAST = 0, CREW = 1;
    Context context;


    public CastAndCrewAdapter(List<Cast> castList, int type, int dummy) {
        this.itemCast = castList;
        this.type = type;
    }

    public CastAndCrewAdapter(List<Crew> crewList, int type) {
        this.itemCrew = crewList;
        this.type = type;
    }

    @Override
    public CastAndCrewAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.crew_cast_view, parent, false);
        context = parent.getContext();
        return new CastAndCrewAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CastAndCrewAdapter.MyViewHolder holder, int position) {
        Cast cast;
        Crew crew;
        if (type == CAST) {
            if (position == 0) {
                holder.header.setText(R.string.cast);
            }
            cast = itemCast.get(position);
            setupInformation(holder, cast);
        } else {
            if (position == 0)
                holder.header.setText(R.string.crew);
            crew = itemCrew.get(position);
            setupInformation(holder, crew);
        }
        if (position != 0)
            holder.header.setVisibility(View.INVISIBLE);
    }

    private void setupInformation(MyViewHolder holder, Crew crew) {
        holder.name.setText("" + crew.getName());
        holder.subtitle.setText("" + crew.getDepartment());
        Utils.loadImage(context, holder.profile, "https://image.tmdb.org/t/p/w500" + crew.getProfilePath(), 0, R.drawable.ic_person_black_24dp);
    }

    private void setupInformation(MyViewHolder holder, Cast cast) {
        holder.name.setText("" + cast.getName());
        holder.subtitle.setText("as " + cast.getCharacter());
        Utils.loadImage(context, holder.profile, "https://image.tmdb.org/t/p/w500" + cast.getProfilePath(), 0, R.drawable.ic_person_black_24dp);

    }

    @Override
    public int getItemCount() {
        if (type == CREW)
            return itemCrew.size();
        else
            return itemCast.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name, subtitle, header;
        ImageView profile;

        public MyViewHolder(View view) {
            super(view);
            init(view);
            setupStyle();
        }

        private void setupStyle() {
            name.setTypeface(FontUtils.getLight());
            subtitle.setTypeface(FontUtils.getLight());
            header.setTypeface(FontUtils.getLight(), BOLD);
        }

        private void init(View view) {
            header = view.findViewById(R.id.header);
            name = view.findViewById(R.id.name);
            subtitle = view.findViewById(R.id.subtitle);
            profile = view.findViewById(R.id.profile);
        }
    }
}
