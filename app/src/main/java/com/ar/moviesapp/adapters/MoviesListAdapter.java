package com.ar.moviesapp.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ar.moviesapp.FontUtils;
import com.ar.moviesapp.R;
import com.ar.moviesapp.Utils;
import com.ar.moviesapp.apis.RestClient;
import com.ar.moviesapp.apis.model.Cast;
import com.ar.moviesapp.apis.model.CastAndCrew;
import com.ar.moviesapp.apis.model.Crew;
import com.ar.moviesapp.apis.model.MoviesInfo;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

import static android.graphics.Typeface.BOLD;

/**
 * Created by abdul on 16/04/18.
 *
 * This is main Adapter which holds the all upcoming or released movie's information;
 * 1. adding every time new list of information into existing list using addAllItem method.
 * 2. we have n number of pages so based on pages getting new list of movies information.
 * 3. setting into view
 * 4. clicking on holder view setting two recycler view one for cast and another for crews;
 */

public class MoviesListAdapter extends RecyclerView.Adapter<MoviesListAdapter.MyViewHolder> {
    private static final String TAG = "MoviesListAdapter";
    private Context context;
    private List<MoviesInfo> list;

    @SuppressLint("StaticFieldLeak")
    public void addAllItem(final List<MoviesInfo> upcomingMoviesList) {
        if (list != null) {
            final int lastIndex = list.size();

            new AsyncTask<Void, Void, List<MoviesInfo>>() {
                @Override
                protected List<MoviesInfo> doInBackground(Void... voids) {
                    list.addAll(upcomingMoviesList);
                    Collections.sort(list);
                    return list;
                }

                @Override
                protected void onPostExecute(List<MoviesInfo> items) {
                    if (context == null)
                        return;
                    notifyItemRangeChanged(lastIndex, getItemCount() - 1);
                    super.onPostExecute(items);
                }
            }.execute();
        }

    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        //public TextView title, popular, votes, releaseDate, description;
        ImageView icon;
//        View viewMore;
//        private RecyclerView castRecycler, crewRecycler;
//        private LinearLayoutManager castManager, crewManager;
//        private CastAndCrewAdapter castAdapter, crewAdapter;
//        private List<Cast> castList;
//        private List<Crew> crewList;


        public MyViewHolder(View view) {
            super(view);
            init(view);
        //    setupStyle();
        }


//        private void setupStyle() {
//            title.setTypeface(FontUtils.getLight(), BOLD);
//            popular.setTypeface(FontUtils.getLight());
//            votes.setTypeface(FontUtils.getLight());
//            description.setTypeface(FontUtils.getLight());
//            releaseDate.setTypeface(FontUtils.getLight());
//        }

        private void init(View view) {
//            title = view.findViewById(R.id.showTitle);
//            popular = view.findViewById(R.id.popular);
//            votes = view.findViewById(R.id.votes);
//            releaseDate = view.findViewById(R.id.releaseDate);
            icon = view.findViewById(R.id.icon);
//            viewMore = view.findViewById(R.id.moreView);
//            description = view.findViewById(R.id.description);

        }

//        private void initCrewAndCast(View view) {
//            castRecycler = view.findViewById(R.id.castRecycler);
//            crewRecycler = view.findViewById(R.id.crewRecycler);
//
//            castList = new ArrayList<>();
//            crewList = new ArrayList<>();
//
//            castAdapter = new CastAndCrewAdapter(castList, CastAndCrewAdapter.CAST, 0);
//            crewAdapter = new CastAndCrewAdapter(crewList, CastAndCrewAdapter.CREW);
//            castManager = new LinearLayoutManager(context, OrientationHelper.HORIZONTAL, false);
//            crewManager = new LinearLayoutManager(context, OrientationHelper.HORIZONTAL, false);
//
//            castRecycler.setLayoutManager(castManager);
//            crewRecycler.setLayoutManager(crewManager);
//        }
    }

    public MoviesListAdapter(Context context, List<MoviesInfo> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movies_list_item_image, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final MoviesInfo item = list.get(position);

//        try {
//            holder.releaseDate.setText(Utils.getDateFormat(item.getReleaseDate()));
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        holder.title.setText(item.getTitle());
        Utils.loadImage(context, holder.icon, "https://image.tmdb.org/t/p/w185" + item.getPosterPath(), 0, 0);
//        holder.votes.setText(item.getVoteCount() + " Votes");
//        holder.popular.setText(Utils.df.format(item.getPopularity()) + "%");

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!item.isExpand) {
                    // holder.initCrewAndCast(holder.itemView);
                    final CompositeDisposable disposable = new CompositeDisposable();
                    // holder.description.setText(item.getOverview());
//                    RestClient.getApiService().getCastAndCrew(item.getId())
//                            .subscribeOn(Schedulers.io())
//                            .observeOn(AndroidSchedulers.mainThread())
//                            .subscribe(new Observer<Response<CastAndCrew>>() {
//                                @Override
//                                public void onSubscribe(Disposable d) {
//                                    disposable.add(d);
//                                }
//
//                                @Override
//                                public void onNext(Response<CastAndCrew> data) {
//                                    Utils.showViews(holder.viewMore);
//                                    holder.castList.addAll(data.body().getCastList());
//                                    holder.crewList.addAll(data.body().getCrewList());
//
//                                    holder.crewRecycler.setAdapter(holder.crewAdapter);
//                                    holder.castRecycler.setAdapter(holder.castAdapter);
//
//                                    holder.crewAdapter.notifyDataSetChanged();
//                                    holder.castAdapter.notifyDataSetChanged();
//                                }
//
//                                @Override
//                                public void onError(Throwable e) {
//
//                                }
//
//                                @Override
//                                public void onComplete() {
//                                    disposable.clear();
//                                }
//                            });
//                } else
//                    Utils.hideViews(holder.viewMore);
//
//                item.isExpand = !item.isExpand;
                }
            }
        });

//        if (item.isExpand)
//            holder.itemView.performClick();
//        else
//            holder.viewMore.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


}
